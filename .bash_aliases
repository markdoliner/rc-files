#!/bin/sh

export EDITOR=vim
export HISTSIZE=100000 # bash and zsh
export HISTFILESIZE=$HISTSIZE # bash
export SAVEHIST=$HISTSIZE # zsh
export HOMEBREW_NO_ANALYTICS=1
export HOMEBREW_NO_ENV_HINTS=1
export POETRY_VIRTUALENVS_IN_PROJECT=true
export RIPGREP_CONFIG_PATH=$HOME/.ripgreprc

# --no-init disables clearing the screen upon exit.
# --RAW-CONTROL-CHARS allows ANSI color codes.
# --quit-if-one-screen is important because using git is unpleasant
#     without it because everything is piped through less, even very
#     short output.
export LESS="--no-init --RAW-CONTROL-CHARS --quit-if-one-screen"

alias df='df -h'
alias du='du -h'
alias rm='rm -i'

function ls () {
  # Use --QUIT-AT-EOF because when viewing directory listings it's more
  # convenient to auto-exit than to have the ability to scroll back up.
  /bin/ls -lhF --color "$@" | less --QUIT-AT-EOF;
}
