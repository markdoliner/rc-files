" Use Vim defaults instead of 100% vi-compatibility
set nocompatible
set backspace=indent,eol,start

" When inserting text, don't add line breaks if the inserted text
" is too long
set textwidth=0

" Temporary write a backup file when saving, but delete it once the
" file has been saved successfully
set nobackup
set writebackup

" Remember information about the last 100 files
" Remember up to 500 lines in a given register
" Remember up to 1000 commands
set viminfo='100,\"500
set history=500

" Always show the cursor position
set ruler

" Turn off highlighting of matching parenthesis
let loaded_matchparen = 1

set backspace=2
set incsearch
set number
set ruler
set tabstop=4

" Use utf-8 in the interface, and default to utf-8 for new files
set encoding=utf-8

" Always show at least 3 lines above and below the cursor
set scrolloff=3

" Highlight redundant whitespaces.
highlight RedundantSpaces ctermbg=red guibg=red
match RedundantSpaces /\s\+$\| \+\ze\t/

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
endif

" Only do this part when compiled with support for autocommands
if has("autocmd")
  " When editing a file, always jump to the last cursor position
  autocmd BufReadPost *
  \ if line("'\"") > 0 && line ("'\"") <= line("$") |
  \   exe "normal! g'\"" |
  \ endif
endif

" Turn off autoindent
set nocindent
set noautoindent
set nosmartindent
set indentexpr=
filetype indent off
filetype plugin indent off
