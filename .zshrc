source ~/.bash_aliases

bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line
